<?php
	session_start();
	include 'core/helper/myHelper.php';
	
	if (!empty($_SESSION['username']) AND !empty($_SESSION['password'])) {
	
		include 'core/init.php';
		
		$tickets = $db->ticket();
		$tasks = $db->task();
		
		$planning = $db->planning()
			->order("created DESC");
		
		$body = 'tickets';
?>

<!doctype html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->
<head>
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <?php include ('_include.php'); ?>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <!--<link rel="shortcut icon" href="/favicon.ico">-->
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="stylesheet" href="dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="dist/css/admin.css">
        <link rel="stylesheet" href="includes/css/style.css">
        <link rel="stylesheet" href="dist/assets/font-awesome/css/font-awesome.css">

        <link rel="stylesheet" href="dist/css/plugins/jquery-select2.min.css">
        <link rel="stylesheet" href="dist/css/plugins/jquery-dataTables.min.css">
        <!--[if lt IE 9]>
        <script src="dist/assets/libs/html5shiv/html5shiv.min.js"></script>
        <script src="dist/assets/libs/respond/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="">
	
		<?php include ('_header.php'); ?>
	
        <div class="page-wrapper">
            <aside class="sidebar sidebar-default">
				
				<?php include('nav.php'); ?>
			
			</aside>

            <div class="page-content">
                <div class="page-subheading page-subheading-md">
					<ol class="breadcrumb">
						<li><a href="javascript:;">Dashboard</a></li>
						<li class="active"><a href="javascript:;">List Ticket</a></li>
					</ol>
				</div>
				<div class="page-heading page-heading-md">
					<h2 class="pull-left">List Ticket</h2>
					<div class="col-button-colors pull-right">
						<a href="dashboard.php" class="btn btn-primary">Back</a>
						<a href="add-ticket.php" class="btn btn-primary">Add New Ticket</a>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="col-lg-12">
					<div class="col-lg-12">
						<form id="AddPlanning" class="form-horizontal form-bordered" role="form">
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="row">
										<div class="col-lg-12">
											<div class="col-button-colors pull-left">
												<h1 style="padding-top:10px;" class="panel-title">List Ticket</h1>
											</div>
										</div>
									</div>
								</div>
						
								<div class="panel-body">
									<table id="table-basic" class="table table-striped">
										<thead>
											<tr>
												<th style="width:30px;">No.</th>
												<th>Subject</th>
												<th>Project Manager</th>
												<th>Project Name</th>
												<th>Created</th>
												<th>Priority</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
										<tbody>
										<?php $no = 1; ?>
										
										<?php foreach ($tickets as $ticket){ ?>
											<tr class="odd gradeX">
												<td><?php echo $no; ?></td>
												<td><?php echo ucfirst($ticket["subject"]); ?></td>
												<td><?php echo $ticket->user['firstname']; ?></td>
												<td><?php echo $ticket->project['name']; ?></td>
												<td><?php echo tgl_indo($ticket['created']); ?></td>
												<td align="center"><?php echo $ticket['priority']; ?></td>
												<td><label  style="display: block;" class="label <?php echo colour_status($ticket['status']); ?>"><?php echo $ticket['status']; ?></label></td>
												<?php if(($_SESSION['user_type']) != 'manajer-proyek'){ ?>
													<td class="btn-group	">
														<a class="btn btn-primary">Modify</a>
														<a class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
															<span class="caret"></span>
															<span class="sr-only">Toggle Dropdown</span>
														</a>
														<ul class="dropdown-menu" role="menu">
															<li><a href="detail-ticket.php?id=<?php echo $ticket['id']; ?>">View</a></li>
															<li><a href="action/delete_ticket.php?id=<?php echo $ticket['id']; ?>" onclick="return confirm('Are you sure you want to Delete ?');">Delete</a></li>
														</ul>
													</td>
												<?php }else{ ?>
													<td class="btn-group" width="">
														<a href="detail-ticket.php?id=<?php echo $ticket['id']; ?>" class="btn btn-info">View</a>
													</td>
												<?php } ?>
											</tr>
										<?php $no++ ?>
										<?php } ?>
										</tbody>
									</table>
								</div>
								
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
        <script src="dist/assets/libs/jquery/jquery.min.js"></script>
        <script src="dist/assets/bs3/js/bootstrap.min.js"></script>
        <script src="dist/assets/plugins/jquery-navgoco/jquery.navgoco.js"></script>
        <script src="dist/js/main.js"></script>

        <!--[if lt IE 9]>
        <script src="dist/assets/plugins/flot/excanvas.min.js"></script>
        <![endif]-->
        <script src="dist/assets/plugins/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="includes/js/script.js"></script>

        <script src="dist/assets/plugins/jquery-datatables/js/jquery.dataTables.js"></script>
        <script src="dist/assets/plugins/jquery-datatables/js/dataTables.tableTools.js"></script>
        <script src="dist/assets/plugins/jquery-datatables/js/dataTables.bootstrap.js"></script>
		<script src="dist/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="dist/assets/plugins/jquery-chosen/chosen.jquery.min.js"></script>
        <script src="dist/assets/plugins/jquery-select2/select2.min.js"></script>
        <script src="includes/js/tables-data-tables.js"></script>
		<script src="dist/assets/plugins/jquery-validation/jquery.validate.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$("#AddPlanning").validate();
			})
		</script>



    </body>
</html>

<?php } ?>