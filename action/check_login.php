<?php

session_start();

require_once '../core/init.php';
require_once '../core/helper/myHelper.php';

$username	= ($_POST['username']);
$password	= (md5($_POST['password']));

	$user = $db->user()->where("username", $username)
			->where("password", $password)
			->where("status", 1);
		
	if($value = $user->fetch()){
		
		$_SESSION['username']  		= $value['username'];
		$_SESSION['firstname']		= $value['firstname'];
		$_SESSION['lastname']		= $value['lastname'];
		$_SESSION['email']   		= $value['email'];
		$_SESSION['password']   	= $value['password'];
		$_SESSION['id']	  			= $value['id'];
		$_SESSION['user_type']		= $value['user_type'];
		
		login_validate();
		
		header('Location: ../dashboard.php');
	}else{
		header('Location: ../index.php');
	}
?>