<?php
	session_start();
	if (empty($_SESSION['username']) AND empty($_SESSION[password])){
		header('location:login.php');
	}else{
		
	include '../core/init.php';	 
	
	$time_now =  date('Y-m-d H:i:s');
	
	$name = $_POST['name'];
	$client = $_POST['client'];
	$category = $_POST['category'];
	$pm = $_POST['project-manager'];
	$description = $_POST['description'];
	$start_date =  date('Y-m-d', strtotime($_POST['start-date']));
	$due_date =  date('Y-m-d', strtotime($_POST['due-date']));
		
	$insert_project = $db->project()->insert(array(
		"name" => $name,
		"client_id" => $client,
		"project_category_id" => $category,
		"user_id" => $pm,
		"description" => $description,
		"start_date" => $start_date,
		"due_date" => $due_date,
		"created" => $time_now,
	));
		
	$insert_project->project();
	
	header ('Location: ../projects.php');
}
?>