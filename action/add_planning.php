<?php
	session_start();
	if (empty($_SESSION['username']) AND empty($_SESSION[password])){
		header('location:login.php');
	}else{
		
	include '../core/init.php';	 
	
	$time_now =  date('Y-m-d H:i:s');
	
	$name 		= $_POST['subject'];
	$project_id 	= $_POST['project-name'];
	$description 	= $_POST['description'];
	
	$start_date =  date('Y-m-d', strtotime($_POST['start-date']));
	$due_date =  date('Y-m-d', strtotime($_POST['due-date']));
		
	$insert_planning = $db->planning()->insert(array(
		"name" 		=> $name,
		"project_id" => $project_id,
		"description" => $description,
		"start_date" => $start_date,
		"due_date" => $due_date,
		"last_update" => $time_now
	));
		
	$insert_planning->planning();
	
	header ('Location: ../planning.php');
}
?>