<?php
if(!isset($_SESSION)){
	
    session_start();
	include '../core/init.php';
	
	$name			=	$_POST['name'];
	$description	=	$_POST['description'];
		
	$id_cat = $_POST['id'];
	
	$category = $db->project_category[$id_cat];
	
	$data = array(
			"id" => $id_cat,
			"name" => $name,
			"description" => $description
		);
		
		$result = $category->update($data);
		
	header ('Location: ../master.php?module=category');
	
}
?>