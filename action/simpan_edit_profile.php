<?php
	session_start();
	include '../core/helper/myHelper.php';
	
	if (!empty($_SESSION['username']) AND !empty($_SESSION['password'])) {
	
		include '../core/init.php'; 

		$firstname=ucwords($_POST['firstname']);
		$lastname=ucwords($_POST['lastname']);
		$username=$_POST['username'];
		$email=$_POST['email'];
		//$jenis_kelamin=$_POST['jenis-kelamin'];
		$phone_number=$_POST['phone_number'];
		$id_user = $_POST['id'];
		
		$user = $db->user[$id_user];
	
	if ($user) {
		$data = array(
			"id" => $id_user,
			"firstname" => $firstname,
			"lastname" => $lastname,
			"username" => $username,
			"email" => $email,
			"phone_number" => $phone_number,
		);
		$result = $user->update($data);
	}
	
	header ('Location: ../edit-profile.php');
	
	}
?>