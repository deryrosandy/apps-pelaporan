<?php
if(!isset($_SESSION)){
    session_start();
	
	include '../core/init.php';
	
	$name 			= $_POST['name'];
	$address 		= $_POST['address'];
	$email 			= $_POST['email'];
	$phone_number 	= $_POST['phone_number'];
	$city 			= $_POST['city'];
	$website 		= $_POST['website'];
	$time_now =  date('Y-m-d h:s');
	
	$id = $_POST['id'];
	
	$client = $db->client[$id];
	
	if ($client) {
		$data = array(
			"id" 			=> $id,
			"name"			=> $name,
			"address" 		=> $address,
			"email" 		=> $email,
			"phone_number" 	=> $phone_number,
			"city" 			=> $city,
			"website" 		=> $website,
			"last_update" 		=> $time_now
		);
		
		$result = $client->update($data);
	}
	
	header ('Location: ../clients.php');
}
?>