<?php
	
	include '../core/init.php';

	$name			=	ucwords($_POST['name']);
	$username		=	$_POST['username'];
	$password		=	md5($_POST['password']);
	$email			=	$_POST['email'];
	$jenis_kelamin	=	$_POST['jenis_kelamin'];
	$phone_number	=	$_POST['phone_number'];
	
	$active = '1';
	
	$user_reg = $db->user()->fetch();
	
	if($user_reg['username'] == $username){
		echo "Username yang anda masukkan sudah terdaftar";
	}else{
	
		$user_login = $db->user()->insert(array(
			"name" => $name,
			"username" => $username,
			"password" => $password,
			"email" => $email,
			"user_group_id" => '2',
			"jenis_kelamin" => $jenis_kelamin,
			"phone_number" => $phone_number,
			"active" => $active
			));
		
		$user_login->update();
		
		header ('Location: ../login.php');
	}
?>