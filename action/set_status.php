<?php
	session_start();
	if (empty($_SESSION['username']) AND empty($_SESSION[password])) {
		header('location:../login.php');
	}else{
	
		include '../core/init.php';
		$id 		= $_POST['id'];
		$status 	= $_POST['status'];
		
		if($_POST){
			//check if its an ajax request, exit if not
			
			if(!isset($_SERVER['HTTP_X_REQUESTED_WITH']) AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) != 'xmlhttprequest') {
				
				$output = json_encode(array( //create JSON data
					'type'=>'error', 
					'text' => 'Sorry Request must be Ajax POST'
				));
				die($output); //exit script outputting json data
			}
			
			$id_ticket = $db->ticket[$id];
		
			if ($id_ticket) {
				$data = array(
					"id" => $id,
					"status" => $status
				);
				$result = $id_ticket->update($data);
			}
						
			$output = json_encode(array('type'=>'message', 'text' => ' Berhasil'));
			die($output);
			
		}
	}	
?>