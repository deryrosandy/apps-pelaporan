<?php
	session_start();
	if (empty($_SESSION['username']) AND empty($_SESSION[password])){
		header('location:login.php');
	}else{
		
	include '../core/init.php';	 
	
	$time_now =  date('Y-m-d H:i:s');
	
	$name = $_POST['name'];
	$planning = $_POST['planning'];
	$user_id = $_SESSION['id'];
	$description = $_POST['description'];
	$start_date =  date('Y-m-d', strtotime($_POST['start-date']));
	$due_date =  date('Y-m-d', strtotime($_POST['due-date']));
		
	$insert_task = $db->task()->insert(array(
		"name" => $name,
		"planning_id" => $planning,
		"user_id" => $user_id,
		"description" => $description,
		"start_date" => $start_date,
		"due_date" => $due_date
	));
		
	$insert_task->task();
	
	header ('Location: ../tasks.php');
}
?>