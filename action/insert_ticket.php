<?php
	session_start();
	if (empty($_SESSION['username']) AND empty($_SESSION[password])){
		header('location:login.php');
	}else{
		
	include '../core/init.php';	 
	
	$time_now =  date('Y-m-d H:i:s');
	
	$subject 		= $_POST['subject'];
	$project_id 	= $_POST['project_id'];
	$task_id 		= $_POST['task_id'];
	$user_id 		= $_SESSION['id'];
	$priority		= $_POST['priority'];
	$description	= $_POST['description'];
	
	$insert_ticket = $db->ticket()->insert(array(
		"subject" => $subject,
		"priority" => $priority,
		"user_id" => $user_id,
		"project_id" => $project_id,
		"task_id" => $task_id,
		"description" => $description,
		"created" => $time_now
	));
		
	$insert_ticket->ticket();
	
	header ('Location: ../tickets.php');
}
?>