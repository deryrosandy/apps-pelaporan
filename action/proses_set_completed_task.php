<?php
	session_start();
	include '../core/helper/myHelper.php';
	
	if (!empty($_SESSION['username']) AND !empty($_SESSION['password'])) {
	
	include '../core/init.php';	 
	
	$time_now =  date('Y-m-d H:i:s');
	
	$user = $_SESSION['id'];
	$id_task = $_POST['id_task'];
	
	$task = $db->task[$id_task];
	
	if ($task) {
		$data = array(
			"id" => $id_task,
			"status" => 'completed',
			"completed_at" => $time_now
		);
		
		$result = $task->update($data);
	}
	/*
	if($user_type == '2'){
		$url = "../detail-my-ticket.php?id=" . $id_ticket;
	}else{
		$url = "../detail-ticket.php?id=" . $id_ticket;
	}
	*/
	$url = "../detail-task.php?id=" . $id_task;
	
	header ("Location: ".$url);
	
	}
?>