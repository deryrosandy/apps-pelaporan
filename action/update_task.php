<?php
if(!isset($_SESSION)){
    session_start();
	
	include '../core/init.php';
	
	$name = $_POST['name'];
	$planning = $_POST['planning'];
	$description = $_POST['description'];
	$start_date =  date('Y-m-d', strtotime($_POST['start-date']));
	$due_date =  date('Y-m-d', strtotime($_POST['due-date']));
	
	$id = $_POST['id'];
	
	$task = $db->task[$id];
	
	if ($task) {
		$data = array(
			"id" => $id,
			"name" => $name,
			"planning_id" => $planning,
			"description" => $description,
			"start_date" => $start_date,
			"due_date" => $due_date
		);
		
		$result = $task->update($data);
	}
	
	header ('Location: ../tasks.php');
}
?>