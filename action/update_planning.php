<?php
if(!isset($_SESSION)){
    session_start();
	
	include '../core/init.php';
	
	$time_now =  date('Y-m-d H:i:s');
	
	$id = $_POST['id'];
	
	$name 			= $_POST['subject'];
	$project_id 	= $_POST['project'];
	$description 	= $_POST['description'];
	
	$start_date 	=  date('Y-m-d', strtotime($_POST['start-date']));
	$due_date 		=  date('Y-m-d', strtotime($_POST['due-date']));
	
	$planning = $db->planning[$id];
	
	if ($planning) {
		$data = array(
			"id" => $id,
			"name" => $name,
			"project_id" => $project_id,
			"description" => $description,
			"start_date" => $start_date,
			"due_date" => $due_date,
			"last_update" => $time_now,
		);
		
		$result = $planning->update($data);
	}
	
	header ('Location: ../planning.php');
}
?>