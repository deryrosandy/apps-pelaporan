<?php
if(!isset($_SESSION)){
    session_start();
	
	include '../core/init.php';
	
	$name = $_POST['name'];
	$client = $_POST['client'];
	$category = $_POST['category'];
	$pm = $_POST['project-manager'];
	$description = $_POST['description'];
	$start_date =  date('Y-m-d', strtotime($_POST['start-date']));
	$due_date =  date('Y-m-d', strtotime($_POST['due-date']));
	
	$id = $_POST['id'];
	
	$project = $db->project[$id];
	
	if ($project) {
		$data = array(
			"id" => $id,
			"client_id" => $client,
			"project_category_id" => $category,
			"user_id" => $pm,
			"description" => $description,
			"start_date" => $start_date,
			"due_date" => $due_date
		);
		
		$result = $project->update($data);
	}
	
	header ('Location: ../projects.php');
}
?>