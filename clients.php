<?php
	session_start();
	include 'core/helper/myHelper.php';
	
	if (!empty($_SESSION['username']) AND !empty($_SESSION['password'])) {
	
		include 'core/init.php';
		
		$clients = $db->client();
		
		$body = 'clients';
?>

<!doctype html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->
<head>
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <?php include ('_include.php'); ?>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <!--<link rel="shortcut icon" href="/favicon.ico">-->
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="stylesheet" href="dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="dist/css/admin.css">
        <link rel="stylesheet" href="includes/css/style.css">
        <link rel="stylesheet" href="dist/assets/font-awesome/css/font-awesome.css">

        <link rel="stylesheet" href="dist/css/plugins/jquery-select2.min.css">
        <link rel="stylesheet" href="dist/css/plugins/jquery-dataTables.min.css">
        <!--[if lt IE 9]>
        <script src="dist/assets/libs/html5shiv/html5shiv.min.js"></script>
        <script src="dist/assets/libs/respond/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="">
		
		<?php include '_header.php'; ?>
		
        <div class="page-wrapper">
            <aside class="sidebar sidebar-default">
				
				<?php include('nav.php'); ?>
			
			</aside>

            <div class="page-content">
                <div class="page-subheading page-subheading-md">
					<ol class="breadcrumb">
						<li><a href="javascript:;">Dashboard</a></li>
						<li class="active"><a href="javascript:;">List Clients</a></li>
					</ol>
				</div>
				<div class="page-heading page-heading-md">
					<h2 class="pull-left">Clients</h2>
					<div class="col-button-colors pull-right">
						<a href="dashboard.php" class="btn btn-primary">Back</a>
						<a href="add-client.php" class="btn btn-primary">Add New Client</a>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="col-lg-12">
					<div class="col-lg-12">
						<form id="AddPlanning" class="form-horizontal form-bordered" role="form">
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="row">
										<div class="col-lg-12">
											<div class="col-button-colors pull-left">
												<h1 style="padding-top:10px;" class="panel-title">List Clients</h1>
											</div>
										</div>
									</div>
								</div>
								<div class="panel-body">
									<table id="table-basic" class="table table-striped">
										<thead>
											<tr>
												<th>No.</th>
												<th>Name</th>
												<th>Email</th>
												<th>Phone</th>
												<th>Address</th>
												<th>City</th>
												<th>Website</th>
												<?php if(($_SESSION['user_type']) != 'manajer-proyek'){ ?>
													<th>Action</th>
												<?php } ?>
											</tr>
										</thead>
										<tbody>
										<?php $n=1; ?>
										<?php foreach ($clients as $client){ ?>
										
												<tr class="odd gradeX">
													<td><?php echo $n; ?></td>
													<td><?php echo $client['name']; ?></td>
													<td><?php echo $client['email']; ?></td>
													<td class="center"><?php echo $client['phone_number']; ?></td>
													<td><?php echo $client['address']; ?></td>
													<td><?php echo $client['city']; ?></td>
													<td><?php echo $client['website']; ?></td>
													<?php if(($_SESSION['user_type']) != 'manajer-proyek'){ ?>
														<td class="btn-group" width="140px">
															<a href="edit-client.php?id=<?php echo $client['id']; ?>" class="btn btn-warning">Edit</a>
															<a href="action/delete_client.php?id=<?php echo $client['id']; ?>" class="btn btn-danger">Delete</a>
														</td>
													<?php } ?>
												</tr>
											
											<?php $n++; ?>
										<?php } ?>
										
										</tbody>
									</table>
								</div>
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
        <script src="dist/assets/libs/jquery/jquery.min.js"></script>
        <script src="dist/assets/bs3/js/bootstrap.min.js"></script>
        <script src="dist/assets/plugins/jquery-navgoco/jquery.navgoco.js"></script>
        <script src="dist/js/main.js"></script>

        <!--[if lt IE 9]>
        <script src="dist/assets/plugins/flot/excanvas.min.js"></script>
        <![endif]-->
        <script src="dist/assets/plugins/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="includes/js/script.js"></script>

        <script src="dist/assets/plugins/jquery-datatables/js/jquery.dataTables.js"></script>
        <script src="dist/assets/plugins/jquery-datatables/js/dataTables.tableTools.js"></script>
        <script src="dist/assets/plugins/jquery-datatables/js/dataTables.bootstrap.js"></script>
		<script src="dist/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="dist/assets/plugins/jquery-select2/select2.min.js"></script>
		<script src="dist/assets/plugins/jquery-chosen/chosen.jquery.min.js"></script>
        <script src="includes/js/tables-data-tables.js"></script>



    </body>
</html>

<?php } ?>