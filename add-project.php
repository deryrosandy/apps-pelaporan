<?php
	session_start();
	include 'core/helper/myHelper.php';
	
	if (!empty($_SESSION['username']) AND !empty($_SESSION['password'])) {
	
		include 'core/init.php';
		
		$clients = $db->client();
		$category = $db->project_category();
		$project_m = $db->user()
						->where("user_type", "manajer-proyek");
		$body = 'projects';
?>
<!doctype html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->
	<head>
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<?php include ('_include.php'); ?>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <!--<link rel="shortcut icon" href="/favicon.ico">-->
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="stylesheet" href="dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="dist/css/admin.css">
        <link rel="stylesheet" href="includes/css/style.css">
        <link rel="stylesheet" href="dist/assets/font-awesome/css/font-awesome.css">

        <link rel="stylesheet" href="dist/css/plugins/jquery-select2.min.css">
		<link rel="stylesheet" href="dist/css/plugins/jquery-chosen.min.css">
        <link rel="stylesheet" href="dist/css/plugins/jquery-dataTables.min.css">
        <!--[if lt IE 9]>
        <script src="dist/assets/libs/html5shiv/html5shiv.min.js"></script>
        <script src="dist/assets/libs/respond/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="">
       
	   <?php include ('_header.php'); ?>
	   
        <div class="page-wrapper">
            <aside class="sidebar sidebar-default">
				
				<?php include('nav.php'); ?>
			
			</aside>

            <div class="page-content">
                <div class="page-subheading page-subheading-md">
					<ol class="breadcrumb">
						<li><a href="javascript:;">Dashboard</a></li>
						<li class="active"><a href="javascript:;">Add New Project</a></li>
					</ol>
				</div>
				<div class="page-heading page-heading-md">
					<h2 class="pull-left">Project</h2>
					<div class="col-button-colors pull-right">
						<a href="projects.php" class="btn btn-primary">Back</a>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="col-sm-12">
					<div class="col-lg-12">
						<form action="action/insert_project.php" name="submit-komentar" method="POST" class="form-horizontal form-bordered" role="form">
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="row">
										<div class="col-lg-12">
											<div class="col-button-colors pull-left">
												<h1 style="padding-top:10px;" class="panel-title">Add New Project</h1>
											</div>
										</div>
									</div>
								</div>
								<div class="panel-body">

									<div class="form-group">
										<label class="control-label col-sm-3">Name</label>

										<div class="controls col-sm-5">
											<input type="text" name="name" class="form-control required" title="Field required" placeholder="Insert Name">
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-sm-3">Client</label>

										<div class="col-lg-3">
											<select class="form-control form-chosen" name="client" data-placeholder="Choose a Client...">
												<option value=""></option>
												<?php foreach($clients as $client){ ?>
													<option value="<?php echo $client['id']; ?>"><?php echo $client['name'];?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-sm-3">Category</label>

										<div class="col-lg-3">
											<select class="form-control form-chosen" name="category" data-placeholder="Choose a Category...">
												<option value=""></option>
												<?php foreach($category as $cat){ ?>
													<option value="<?php echo $cat['id']; ?>"><?php echo $cat['name'];?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-sm-3">Start Date</label>

										<div class="controls col-lg-2">
											<input type="text" name="start-date" placeholder="Set Start Date" class="form-control" data-rel="datepicker"/>
										</div>
										
									</div>
									
									<div class="form-group">
										<label class="control-label col-lg-3">Due Date</label>
										
										<div class="controls col-sm-2">
											<input type="text" name="due-date" class="form-control required" placeholder="Set Due Date" title="Field Required" data-rel="datepicker"/>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-sm-3">Project Manager</label>

										<div class="col-lg-3">
											<select class="form-control form-chosen required" title="Field Required" name="project-manager" data-placeholder="Choose a Project Manager...">
												<option value=""></option>
												<?php foreach($project_m as $pm){ ?>
													<option value="<?php echo $pm['id']; ?>"><?php echo $pm['firstname'];?></option>
												<?php } ?>
											</select>
										</div>
									</div>
									
									<div class="form-group">
										<label class="control-label col-sm-3">Description</label>

										<div class="controls col-sm-5">
											<textarea rows="4" class="form-control" name="description" id="" placeholder="Enter Description (Optional)" style="resize:vertical"></textarea>
										</div>
									</div>
								
									<div class="form-group">
										<label class="control-label col-lg-3"></label>
										<div class="controls col-lg-6">
											<button type="submit" class="btn btn-primary">Submit</button>&nbsp;&nbsp;&nbsp;
											<button type="reset" class="btn btn-primary">Reset</button>
										</div>
									</div>
									
								</div>
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
        <script src="dist/assets/libs/jquery/jquery.min.js"></script>
        <script src="dist/assets/bs3/js/bootstrap.min.js"></script>
        <script src="dist/assets/plugins/jquery-navgoco/jquery.navgoco.js"></script>
        <script src="dist/js/main.js"></script>

        <!--[if lt IE 9]>
        <script src="dist/assets/plugins/flot/excanvas.min.js"></script>
        <![endif]-->
        <script src="dist/assets/plugins/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="dist/assets/plugins/jquery-datatables/js/jquery.dataTables.js"></script>
        <script src="dist/assets/plugins/jquery-datatables/js/dataTables.tableTools.js"></script>
        <script src="dist/assets/plugins/jquery-datatables/js/dataTables.bootstrap.js"></script>
        <script src="dist/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="dist/assets/plugins/jquery-select2/select2.min.js"></script>
		<script src="dist/assets/plugins/jquery-chosen/chosen.jquery.min.js"></script>
        <script src="includes/js/tables-data-tables.js"></script>
		<script src="dist/assets/plugins/jquery-validation/jquery.validate.min.js"></script>
		<script src="includes/js/script.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$("#userAddForm").validate();
			})
		</script>


    </body>
</html>

<?php } ?>