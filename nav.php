<?php
	$user_type = $_SESSION['user_type'];	
?>

 <div class="sidebar-profile">
	<div class="profile-body">
		<h4>Welcome, <?php echo ucfirst($_SESSION['username']); ?></h4>
		<br/>
	</div>
</div>

<nav>
	<h5 class="sidebar-header">Navigation</h5>
	<ul class="nav nav-pills nav-stacked">
		<li class="nav <?php if($body == 'dashboard'){echo 'active';}?>">
			<a href="index.php" title="Dashboards">
				<i class="fa fa-lg fa-fw fa-home"></i> Dashboards
			</a>
		</li>
		
		<li class="nav <?php if($body == 'projects'){echo 'active';}?>">
			<a href="projects.php" title="Create New Request">
				<i class="fa fa-lg fa-fw fa-cube"></i> Projects
			</a>
		</li>
		
		<li class="nav <?php if($body == 'tasks'){echo 'active'; } ?>">
			<a href="tasks.php" title="Request">
				<i class="fa fa-lg fa-fw fa-tasks"></i> Tasks<!--<span class="label label-warning pull-right">31</span>-->
			</a>
		</li>
		
		<li class="nav <?php if($body == 'tickets'){echo 'active'; } ?>">
			<a href="tickets.php" title="Request">
				<i class="fa fa-lg fa-fw fa-tags"></i> Tickets<!--<span class="label label-warning pull-right">31</span>-->
				<?php $count_ticket = count($db->ticket()
								->where("status", "assigned"));
				?>
				<?php if(($count_ticket > 0) && (($_SESSION['user_type']) != 'manajer-proyek')){ ?>
					<span class="label label-warning pull-right"><?php echo $count_ticket; ?></span>
				<?php } ?>
			</a>
		</li>
		
		<li class="nav <?php if($body == 'clients'){echo 'active'; } ?>">
			<a href="clients.php" title="Users">
				<i class="fa fa-lg fa-fw fa-users"></i> Clients
			</a>
		</li>
		
		<li class="nav <?php if($body == 'planning'){echo 'active'; } ?>">
			<a href="planning.php" title="Report">
				<i class="fa fa-lg fa-fw fa-list-alt"></i> Planning
			</a>
		</li>
		
		<li class="nav <?php if($body == 'ganti-password'){echo 'active'; } ?>">
			<a href="change-password.php" title="Change Password">
				<i class="fa fa-lg fa-fw fa-folder"></i> Change Password
			</a>
		</li>
		
		<?php if($user_type == 'administrator'){ ?>
		
		<li class="nav <?php if($body == 'master'){echo 'active'; } ?>">
			<a href="master.php" title="Master">
				<i class="fa fa-lg fa-fw fa-gears"></i> Master
			</a>
		</li>
		<?php } ?>
		
	</ul>
</nav>