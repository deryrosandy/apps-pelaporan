/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50617
Source Host           : 127.0.0.1:3306
Source Database       : si_pelaporan_db

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-08-30 08:35:02
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for client
-- ----------------------------
DROP TABLE IF EXISTS `client`;
CREATE TABLE `client` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `phone_number` varchar(13) DEFAULT NULL,
  `address` text,
  `city` varchar(20) DEFAULT NULL,
  `website` varchar(50) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of client
-- ----------------------------
INSERT INTO `client` VALUES ('1', 'PT. Maju Mundur Cantik', 'maju@maju.com', '085710011524', 'Pondok Kopi', 'Jakarta Timur', 'http://majumundurcantik.com', '2016-08-21 02:53:46', '2016-08-27 12:14:00');

-- ----------------------------
-- Table structure for planning
-- ----------------------------
DROP TABLE IF EXISTS `planning`;
CREATE TABLE `planning` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `description` text,
  `created` timestamp NULL DEFAULT NULL,
  `last_update` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of planning
-- ----------------------------
INSERT INTO `planning` VALUES ('1', 'Planning 3', '2016-08-25', '2016-08-24', '1', '1', 'okokok', '2016-08-25 02:24:45', '2016-08-26 22:26:19');
INSERT INTO `planning` VALUES ('2', 'Planning 2', '2016-08-27', '2016-09-03', '1', null, 'Deskripsi Disini', null, '2016-08-26 22:14:30');

-- ----------------------------
-- Table structure for project
-- ----------------------------
DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `description` text,
  `start_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `project_category_id` bigint(20) DEFAULT NULL,
  `status` enum('assigned','on process','completed') DEFAULT 'assigned',
  `user_id` bigint(20) DEFAULT NULL,
  `client_id` bigint(20) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of project
-- ----------------------------
INSERT INTO `project` VALUES ('1', 'Pembangunan Gedung Sekolah', 'Deskripsi Di Sini', '2016-08-25', '2016-08-25', '1', '', '3', '1', '2016-08-21 02:53:08');
INSERT INTO `project` VALUES ('2', 'Project Kedua', 'Deksripsi Project Kedua', '2016-08-25', '2017-03-27', '1', 'assigned', '2', '1', '2016-08-25 02:44:47');

-- ----------------------------
-- Table structure for project_category
-- ----------------------------
DROP TABLE IF EXISTS `project_category`;
CREATE TABLE `project_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of project_category
-- ----------------------------
INSERT INTO `project_category` VALUES ('1', 'Konstruksi', 'Konstruksi Bangunan');

-- ----------------------------
-- Table structure for task
-- ----------------------------
DROP TABLE IF EXISTS `task`;
CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `planning_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `status` enum('not yet','on progress','completed') DEFAULT 'not yet',
  `start_date` date DEFAULT NULL,
  `due_date` date DEFAULT NULL,
  `description` text,
  `created` timestamp NULL DEFAULT NULL,
  `completed_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of task
-- ----------------------------
INSERT INTO `task` VALUES ('1', 'Penggalian Tanah', '1', '1', 'completed', '2016-08-25', '2017-01-11', 'Deskripsi Di Siniii....', '2016-08-25 03:31:30', null);
INSERT INTO `task` VALUES ('2', 'Tahap Kedua', '1', '1', 'completed', '2016-08-27', '2016-08-30', 'Oke', null, '2016-08-28 22:37:55');
INSERT INTO `task` VALUES ('3', 'Tahap Ketiga', '1', '1', 'completed', '2016-08-31', '2016-09-09', 'Deskripsi Task Disini', null, null);
INSERT INTO `task` VALUES ('4', 'Task Pertama', '2', '1', 'completed', '2016-08-27', '2016-08-29', 'Okok', null, null);
INSERT INTO `task` VALUES ('5', 'Task Kedua', '2', '1', 'not yet', '2016-08-31', '2016-09-07', 'SIpp', null, null);

-- ----------------------------
-- Table structure for ticket
-- ----------------------------
DROP TABLE IF EXISTS `ticket`;
CREATE TABLE `ticket` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `subject` varchar(50) DEFAULT NULL,
  `status` enum('assigned','on process','resolved','cancel') DEFAULT 'assigned',
  `user_id` bigint(20) DEFAULT NULL,
  `priority` enum('Urgent','High','Normal','Low') DEFAULT NULL,
  `project_id` bigint(20) DEFAULT NULL,
  `task_id` bigint(20) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ticket
-- ----------------------------
INSERT INTO `ticket` VALUES ('1', 'Test Ticket', 'resolved', '1', 'High', '1', '1', '2016-08-25 04:41:23', 'blablablaaa');
INSERT INTO `ticket` VALUES ('6', 'lagi ticket nya', 'assigned', '1', 'High', '2', '1', '2016-08-25 22:00:23', 'sadfadad');

-- ----------------------------
-- Table structure for ticket_comment
-- ----------------------------
DROP TABLE IF EXISTS `ticket_comment`;
CREATE TABLE `ticket_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ticket_id` bigint(20) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of ticket_comment
-- ----------------------------
INSERT INTO `ticket_comment` VALUES ('2', '1', '1', '2016-08-28 21:37:53', 'OKe,, Segera Di Proses');
INSERT INTO `ticket_comment` VALUES ('3', null, '3', '2016-08-28 21:57:54', null);
INSERT INTO `ticket_comment` VALUES ('4', null, '3', '2016-08-28 22:12:15', null);
INSERT INTO `ticket_comment` VALUES ('5', null, '3', '2016-08-28 22:16:35', null);
INSERT INTO `ticket_comment` VALUES ('6', null, '1', '2016-08-28 22:29:40', null);
INSERT INTO `ticket_comment` VALUES ('7', null, '1', '2016-08-28 22:30:53', null);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) DEFAULT NULL,
  `firstname` varchar(20) DEFAULT NULL,
  `lastname` varchar(20) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `email` varchar(20) DEFAULT NULL,
  `phone_number` varchar(13) DEFAULT NULL,
  `status` tinyint(4) DEFAULT NULL,
  `user_type` enum('administrator','manajer-proyek','kontraktor') DEFAULT NULL,
  `created` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', 'admin', 'Admin', 'Super', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.com', '085710011524', '1', 'administrator', '2016-08-21 00:55:36', '2016-08-21 00:55:38');
INSERT INTO `user` VALUES ('2', 'budianto', 'Budianto', null, '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.com', '085710011524', '1', 'manajer-proyek', '2016-08-25 04:03:42', '2016-08-25 04:03:45');
INSERT INTO `user` VALUES ('3', 'ricky', 'Ricky', 'Lisandy', '21232f297a57a5a743894a0e4a801fc3', 'admin@admin.com', '085710011524', '1', 'manajer-proyek', '2016-08-25 04:03:42', null);
