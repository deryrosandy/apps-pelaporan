<?php
	session_start();
	
	if (!empty($_SESSION['username']) AND !empty($_SESSION['password'])){
	
		include 'core/init.php';
		include 'core/helper/myHelper.php';

		$id = $_GET['id'];

		$cat_edit = $db->project_category()->where("id", $id)->fetch();
		
		$body = 'master';

?>

<!doctype html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->
	<head>
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<?php include ('_include.php'); ?>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <!--<link rel="shortcut icon" href="/favicon.ico">-->
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="stylesheet" href="dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="dist/css/admin.css">
        <link rel="stylesheet" href="includes/css/style.css">
        <link rel="stylesheet" href="dist/assets/font-awesome/css/font-awesome.css">

        <link rel="stylesheet" href="dist/css/plugins/jquery-select2.min.css">
		<link rel="stylesheet" href="dist/css/plugins/jquery-chosen.min.css">
        <link rel="stylesheet" href="dist/css/plugins/jquery-dataTables.min.css">
        <!--[if lt IE 9]>
        <script src="dist/assets/libs/html5shiv/html5shiv.min.js"></script>
        <script src="dist/assets/libs/respond/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="">
       
	   <?php include ('_header.php'); ?>
	   
        <div class="page-wrapper">
            <aside class="sidebar sidebar-default">
				
				<?php include('nav.php'); ?>
			
			</aside>

             <div class="page-content">
                <div class="page-subheading page-subheading-md">
					<ol class="breadcrumb">
						<li><a href="javascript:;">Dashboard</a></li>
						<li class="active"><a href="javascript:;">Edit Category</a></li>
					</ol>
				</div>
				<div class="page-heading page-heading-md">
					<h2 class="pull-left">Category</h2>
					<div class="col-button-colors pull-right">
						<a href="master.php?module=category" class="btn btn-primary">Back</a>
					</div>
					<div class="clearfix"></div>
				</div>
				
				<div class="col-sm-12">
					<div class="col-lg-12">
						<form action="action/update_category.php" name="form-tambah-pengguna" method="POST" class="form-horizontal form-bordered" role="form">
							<div class="panel panel-default">
								<div class="panel-heading">
									<h4 class="panel-title">Edit Category</h4>
								</div>
								<div class="panel-body">

									<div class="form-group">
										<label class="control-label col-sm-3">Name</label>

										<div class="controls col-sm-6">
											<input type="text" name="name" class="form-control" value="<?php echo $cat_edit['name']; ?>">
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-sm-3">Description</label>

										<div class="controls col-sm-6">
											<input type="text" name="description" class="form-control" value="<?php echo $cat_edit['description']; ?>">
										</div>
									</div>
								
									<div class="form-group">
										<div class="controls col-sm-6 col-sm-offset-3">
											<button type="submit" class="btn btn-primary">Update</button>&nbsp;&nbsp;&nbsp;
											<input type="hidden" name="id" value="<?php echo $cat_edit['id']; ?>"/>
										</div>
									</div>
									
								</div>
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
		
		<script src="dist/assets/libs/jquery/jquery.min.js"></script>
        <script src="dist/assets/bs3/js/bootstrap.min.js"></script>
        <script src="dist/assets/plugins/jquery-navgoco/jquery.navgoco.js"></script>
        <script src="dist/js/main.js"></script>

        <!--[if lt IE 9]>
        <script src="dist/assets/plugins/flot/excanvas.min.js"></script>
        <![endif]-->
        <script src="dist/assets/plugins/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="dist/assets/plugins/jquery-datatables/js/jquery.dataTables.js"></script>
        <script src="dist/assets/plugins/jquery-datatables/js/dataTables.tableTools.js"></script>
        <script src="dist/assets/plugins/jquery-datatables/js/dataTables.bootstrap.js"></script>
        <script src="dist/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="dist/assets/plugins/jquery-select2/select2.min.js"></script>
		<script src="dist/assets/plugins/jquery-chosen/chosen.jquery.min.js"></script>
        <script src="includes/js/tables-data-tables.js"></script>
		<script src="dist/assets/plugins/jquery-validation/jquery.validate.min.js"></script>
		<script src="includes/js/script.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$("#userAddForm").validate();
			})
		</script>
    </body>
</html>
<?php } ?>