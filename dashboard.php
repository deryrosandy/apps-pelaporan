<?php
	session_start();
	include 'core/helper/myHelper.php';
	
	if (!empty($_SESSION['username']) AND !empty($_SESSION['password'])){
		
		include 'core/init.php';
		
			$user_id = $db->user("id", $_SESSION['id'])->fetch();
			$total_project = $db->project();
			$total_task = $db->task();
			$total_ticket = $db->ticket();
			$total_planning = $db->planning();
		
		$body = 'dashboard';
?>
<!doctype html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->
<head>
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <?php include ('_include.php'); ?>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <link rel="shortcut icon" href="/favicon.ico">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="stylesheet" href="dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="dist/css/admin.css">
        <link rel="stylesheet" href="includes/css/style.css">
        <link rel="stylesheet" href="dist/assets/font-awesome/css/font-awesome.css">

        <link rel="stylesheet" href="dist/assets/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.css"/>
        <link rel="stylesheet" href="dist/css/plugins/rickshaw.min.css">
        <link rel="stylesheet" href="dist/css/plugins/morris.min.css">

    </head>
    <body class="">
		
		<?php include ('_header.php'); ?>
		
        <div class="page-wrapper">
            <aside class="sidebar sidebar-default">
                
				<?php include('nav.php'); ?>
				
            </aside>

            <div class="page-content">
                <div class="page-subheading page-subheading-md">
					<ol class="breadcrumb">
						<li class="active"><a href="javascript:;">Dashboard</a></li>
					</ol>
				</div>

				<div class="container-fluid-md">
					<div class="row">
					
						<div class="col-sm-6 col-lg-3">
							<div class="panel panel-metric panel-metric-sm">
								<div class="panel-body panel-body-primary">
								
								   <div class="metric-content metric-icon">
										<div class="value">
											<?php echo count($total_project); ?>
										</div>
										<div class="icon">
											<i class="fa fa-cube"></i>
										</div>
										<header>
											<h3 class="thin">Project</h3>
										</header>
									</div>
								</div>
							</div>
						</div>
						<div class="col-sm-6 col-lg-3">
							<div class="panel panel-metric panel-metric-sm">
								<div class="panel-body panel-body-success">
								
								   <div class="metric-content metric-icon">
										<div class="value">
											<?php echo count($total_task); ?>
										</div>
										<div class="icon">
											<i class="fa fa-tasks"></i>
										</div>
										<header>
											<h3 class="thin">Task</h3>
										</header>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-sm-6 col-lg-3">
							<div class="panel panel-metric panel-metric-sm">
								<div class="panel-body panel-body-inverse">
								
								   <div class="metric-content metric-icon">
										<div class="value">
											<?php echo count($total_ticket); ?>
										</div>
										<div class="icon">
											<i class="fa fa-tags"></i>
										</div>
										<header>
											<h3 class="thin">Ticket</h3>
										</header>
									</div>
								</div>
							</div>
						</div>
						
						<div class="col-sm-6 col-lg-3">
							<div class="panel panel-metric panel-metric-sm">
								<div class="panel-body panel-body-info">
								
								   <div class="metric-content metric-icon">
										<div class="value">
											<?php echo count($total_planning); ?>
										</div>
										<div class="icon">
											<i class="fa fa-list-alt"></i>
										</div>
										<header>
											<h3 class="thin">Planning</h3>
										</header>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
        <script src="dist/assets/libs/jquery/jquery.min.js"></script>
        <script src="dist/assets/bs3/js/bootstrap.min.js"></script>
        <script src="dist/assets/plugins/jquery-navgoco/jquery.navgoco.js"></script>
        <script src="dist/js/main.js"></script>

        <!--[if lt IE 9]>
        <script src="dist/assets/plugins/flot/excanvas.min.js"></script>
        <![endif]-->
        <script src="dist/assets/plugins/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="includes/js/script.js"></script>

        <script src="dist/assets/plugins/jquery-jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
        <script src="dist/assets/plugins/jquery-jvectormap/maps/world_mill_en.js"></script>

        <!--[if gte IE 9]>-->
        <script src="dist/assets/plugins/rickshaw/js/vendor/d3.v3.js"></script>
        <script src="dist/assets/plugins/rickshaw/rickshaw.min.js"></script>
        <!--<![endif]-->

        <script src="dist/assets/plugins/flot/jquery.flot.js"></script>
        <script src="dist/assets/plugins/flot/jquery.flot.resize.js"></script>
        <script src="dist/assets/plugins/raphael/raphael-min.js"></script>
        <script src="dist/assets/plugins/morris/morris.min.js"></script>
        <script src="includes/js/dashboard.js"></script>
    </body>
</html>
<?php } ?>