<?php
	session_start();
	include 'core/helper/myHelper.php';
	
	if (!empty($_SESSION['username']) AND !empty($_SESSION['password'])) {
	
		include 'core/init.php';
		
		$id = $_GET['id'];
		$client = $db->client()
					->where("id", $id)
					->fetch();
					
		$body = 'clients';
?>
<!doctype html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->
	<head>
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<?php include ('_include.php'); ?>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <!--<link rel="shortcut icon" href="/favicon.ico">-->
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="stylesheet" href="dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="dist/css/admin.css">
        <link rel="stylesheet" href="includes/css/style.css">
        <link rel="stylesheet" href="dist/assets/font-awesome/css/font-awesome.css">

        <link rel="stylesheet" href="dist/css/plugins/jquery-select2.min.css">
		<link rel="stylesheet" href="dist/css/plugins/jquery-chosen.min.css">
        <link rel="stylesheet" href="dist/css/plugins/jquery-dataTables.min.css">
        <!--[if lt IE 9]>
        <script src="dist/assets/libs/html5shiv/html5shiv.min.js"></script>
        <script src="dist/assets/libs/respond/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="">
       
	   <?php include ('_header.php'); ?>
	   
        <div class="page-wrapper">
            <aside class="sidebar sidebar-default">
				
				<?php include('nav.php'); ?>
			
			</aside>

            <div class="page-content">
                <div class="page-subheading page-subheading-md">
					<ol class="breadcrumb">
						<li><a href="javascript:;">Dashboard</a></li>
						<li class="active"><a href="javascript:;">Edit Client</a></li>
					</ol>
				</div>
				<div class="page-heading page-heading-md">
					<h2 class="pull-left">Client</h2>
					<div class="col-button-colors pull-right">
						<a href="clients.php" class="btn btn-primary">Back</a>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="col-sm-12">
					<div class="col-lg-12">
						<form id="AddClient" action="action/update_client.php" name="form-tambah-pengguna" method="POST" class="form-horizontal form-bordered" role="form">
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="row">
										<div class="col-lg-12">
											<div class="col-button-colors pull-left">
												<h1 style="padding-top:10px;" class="panel-title">Edit Client</h1>
											</div>
										</div>
									</div>
								</div>
								<div class="panel-body">

									<div class="form-group">
										<label class="control-label col-sm-3">Name</label>

										<div class="controls col-sm-5">
											<input type="text" name="name" value="<?php echo $client['name']; ?>" class="form-control required" title="Field Required" placeholder="Insert Name">
										</div>
									</div>
								
									<div class="form-group">
										<label class="control-label col-sm-3">Address</label>

										<div class="controls col-sm-5">
											<input type="text" name="address" value="<?php echo $client['address']; ?>"  class="form-control required" title="Field Required" placeholder="Insert Address">
										</div>
									</div>
								
									<div class="form-group">
										<label class="control-label col-sm-3">City</label>

										<div class="controls col-sm-5">
											<input type="text" name="city" value="<?php echo $client['city']; ?>"  class="form-control required" title="Field Required" placeholder="Insert City">
										</div>
									</div>
								
									<div class="form-group">
										<label class="control-label col-sm-3">Phone</label>

										<div class="controls col-sm-5">
											<input type="text" name="phone_number" value="<?php echo $client['phone_number']; ?>"  class="form-control required" title="Field Required" placeholder="Insert Phone Number">
										</div>
									</div>
								
									<div class="form-group">
										<label class="control-label col-sm-3">Email</label>

										<div class="controls col-sm-5">
											<input type="email" name="email" value="<?php echo $client['email']; ?>"  class="form-control required" title="Field Required" placeholder="Insert Email">
										</div>
									</div>
								
									<div class="form-group">
										<label class="control-label col-sm-3">Website</label>

										<div class="controls col-sm-5">
											<input type="text" name="website" value="<?php echo $client['website']; ?>"  class="form-control" placeholder="Insert Website (Optional)">
										</div>
									</div>
								
									<div class="form-group">
										<label class="control-label col-lg-3"></label>
										<input type="hidden" name="id" value="<?php echo $client['id']; ?>">
										<div class="controls col-lg-6">
											<button type="submit" class="btn btn-primary">Update</button>&nbsp;&nbsp;&nbsp;
											<button type="reset" class="btn btn-primary">Reset</button>
										</div>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
            </div>
        </div>
        <script src="dist/assets/libs/jquery/jquery.min.js"></script>
        <script src="dist/assets/bs3/js/bootstrap.min.js"></script>
        <script src="dist/assets/plugins/jquery-navgoco/jquery.navgoco.js"></script>
        <script src="dist/js/main.js"></script>

        <!--[if lt IE 9]>
        <script src="dist/assets/plugins/flot/excanvas.min.js"></script>
        <![endif]-->
        <script src="dist/assets/plugins/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="dist/assets/plugins/jquery-datatables/js/jquery.dataTables.js"></script>
        <script src="dist/assets/plugins/jquery-datatables/js/dataTables.tableTools.js"></script>
        <script src="dist/assets/plugins/jquery-datatables/js/dataTables.bootstrap.js"></script>
        <script src="dist/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="dist/assets/plugins/jquery-select2/select2.min.js"></script>
		<script src="dist/assets/plugins/jquery-chosen/chosen.jquery.min.js"></script>
        <script src="includes/js/tables-data-tables.js"></script>
		<script src="dist/assets/plugins/jquery-validation/jquery.validate.min.js"></script>
		<script src="includes/js/script.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$("#AddClient").validate();
			})
		</script>


    </body>
</html>

<?php } ?>