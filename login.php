<!doctype html>
<html class="no-js">
	<head>
		<!-- Meta, title, CSS, favicons, etc. -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<?php include('_include.php'); ?>
		<meta name="description" content="">
		<meta name="viewport" content="width=device-width">
		<!--<link rel="shortcut icon" href="/favicon.ico">-->
		<link rel="stylesheet" href="dist/css/bootstrap.min.css">
		<link rel="stylesheet" href="dist/css/admin.css">
		<link rel="stylesheet" href="includes/css/style.css">
		<link rel="stylesheet" href="dist/assets/font-awesome/css/font-awesome.css">
    </head>
    <body class="body-sign-in">
    <div class="container">
        <div class="panel panel-default form-container">
            <div class="panel-body">
                <form id="loginForm" role="form" action="action/check_login.php" method="POST">
					
					<h3 class="text-center margin-xl-bottom">Login System</h3>

                    <div class="form-group text-center">
                        <label class="sr-only"" for="username">Username</label>
                        <input type="username" name="username" class="form-control input-lg required" title="Username Harus Di Isi" id="username" placeholder="Username">
                    </div>
                    <div class="form-group text-center">
                        <label class="sr-only required" for="password">Password</label>
                        <input type="password" name="password" class="form-control input-lg required" title="Password Harus Di Isi" id="password" placeholder="Password">
                    </div>
                    
					<input type="submit" value="Sign In" class="btn btn-primary btn-block btn-lg">
                
				</form>
            </div>
            <div class="panel-body text-center">
                <div class="margin-bottom">
                    <a class="text-muted text-underline" href="javascript:;">Forgot Password?</a>
                </div>
				<!--
				<div>
					Belum Punya Akun ?<a class="text-primary-dark" href="form-daftar-pelanggan.php">Daftar Sekarang!</a>
                </div>
				-->
            </div>
        </div>
    </div>
		<script src="dist/assets/libs/jquery/jquery.min.js"></script>
		<script src="dist/assets/plugins/jquery-validation/jquery.validate.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$("#loginForm").validate();
			})
		</script>
	</body>
</html>
