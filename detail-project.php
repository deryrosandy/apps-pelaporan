<?php
	session_start();
	include 'core/helper/myHelper.php';
	
	if (!empty($_SESSION['username']) AND !empty($_SESSION['password'])) {
	
		include 'core/init.php';

		$id = $_GET['id'];
		
		$project = $db->project()->where("id", $id)->fetch();
		$pm = $db->project_assign()
				->where("id", $project["id"])
				->fetch();
		
		$planning = $db->planning()
			->where("project_id", $project['id'])
			->order("created DESC");
		
		$total = 0;
		$count_task = 0;
		foreach ($planning as $plan){
			$tasks_item = $db->task()->where("planning_id", $plan['id'])
								->order("created DESC");
			$completed_plan = count($db->task()->where("planning_id", $plan['id'])->where("status","completed"));
			$progress_plan = (($completed_plan / (count($tasks_item))) * 100);
			$total += ceil($progress_plan);
			$count_task += count($tasks_item);
		}
		$progress_all = ($total / count($planning));
		
		$category = $db->project_category();
		$body = 'projects';
?>

<!doctype html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->
	<head>
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<?php include ('_include.php'); ?>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <!--<link rel="shortcut icon" href="/favicon.ico">-->
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="stylesheet" href="dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="dist/css/admin.css">
        <link rel="stylesheet" href="includes/css/style.css">
        <link rel="stylesheet" href="dist/assets/font-awesome/css/font-awesome.css">

        <link rel="stylesheet" href="dist/css/plugins/jquery-select2.min.css">
        <link rel="stylesheet" href="dist/css/plugins/jquery-dataTables.min.css">
        <!--[if lt IE 9]>
        <script src="dist/assets/libs/html5shiv/html5shiv.min.js"></script>
        <script src="dist/assets/libs/respond/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="">
	   <?php include ('_header.php'); ?>
	   
        <div class="page-wrapper">
            <aside class="sidebar sidebar-default">
				
				<?php include('nav.php'); ?>
			
			</aside>

            <div class="page-content">
                <div class="page-subheading page-subheading-md">
					<ol class="breadcrumb">
						<li><a href="javascript:;">Dashboard</a></li>
						<li class="active"><a href="javascript:;">Detail Project</a></li>
					</ol>
				</div>
				<div class="page-heading page-heading-md">
					<h2 class="pull-left">Detail Project</h2>
					<div class="col-button-colors pull-right">
						<a href="projects.php" class="btn btn-primary">Back</a>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="col-sm-12">
					<div class="col-lg-12">
						<form action="action/proses_komentar.php" name="submit-komentar" method="POST" class="form-horizontal form-bordered" role="form">
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="row">
										<div class="col-lg-12">
											<div class="col-button-colors pull-left">
												<h1 style="padding-top:10px;" class="panel-title">Detail Project</h1>
											</div>
										</div>
									</div>
								</div>
								<div class="panel-body">
									<div id="detail-request-karyawan" style="padding:20px;" class="form-group">
										<div class="form-group">
											<label class="control-label col-sm-3">Project Name :</label>
											
											<div class="controls col-sm-3">
												<label class="control-label"><?php echo $project['name']; ?></label>
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-sm-3">Category :</label>

											<div class="controls col-sm-3">
												<label class="control-label"><?php echo $project->project_category['name']; ?></label>
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-sm-3">Project Manager :</label>

											<div class="controls col-sm-3">
												<label class="control-label"><?php echo $project->user["firstname"]; ?></label>
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-sm-3">Client :</label>

											<div class="controls col-sm-3">
												<label class="control-label"><?php echo $project->client['name']; ?></label>
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-sm-3">Start Date :</label>

											<div class="controls col-sm-3">
												<label class="control-label"><?php echo tgl_indo($project['start_date']); ?></label>
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-sm-3">Due Date :</label>

											<div class="controls col-sm-3">
												<label class="control-label"><?php echo tgl_indo($project['due_date']); ?></label>
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-sm-3">Description :</label>

											<div class="controls col-sm-3">
												<text class="control-label" style="text-align: left;"><?php echo $project['description']; ?></text>
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-sm-3">Progress :</label>

											<div class="controls col-sm-7">
												<div class="progress progress-lg">
													<?php if($progress_all <= 0){ ?>
														<div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%">
															<span class="">Not Yet</span>
														</div>
													<?php }else{ ?>
														<div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="<?php echo $progress_all; ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo $progress_all; ?>%">
															<span class=""><?php echo ceil($progress_all); ?>%</span>
														</div>
													<?php } ?>
												</div>
											</div>
										</div>
										
										<div class="col-lg-10">
											<div class="col-lg-6">
												<h3 class="pull-left">List Planning</h3>
											</div>
											<table id="table-basic" class="table table-striped">
												<thead>
													<tr>
														<th style="width:30px;">No.</th>
														<th>Name</th>
														<th>Start Date</th>
														<th>Due Date</th>
														<th>Progress (%)</th>
														<th>Action</th>
													</tr>
												</thead>
												<tbody>
												<?php $no = 1; ?>
												<?php if(count($planning) > 0){ ?>
													<?php $total = 0; ?>
													<?php $count_task = 0; ?>
													<?php foreach ($planning as $plan){ ?>
													<?php
														$tasks_item = $db->task()->where("planning_id", $plan['id'])
																			->order("created DESC");
														
														$completed_plan = count($db->task()->where("planning_id", $plan['id'])->where("status","completed"));
														$progress_plan = (($completed_plan / (count($tasks_item))) * 100);
														$total += ceil($progress_plan);
														$count_task = count($tasks_item);
													?>
														<tr class="odd gradeX">
															<td><?php echo $no; ?></td>
															<td><?php echo ucfirst($plan["name"]); ?></td>
															<td><?php echo tgl_indo($plan['start_date']); ?></td>
															<td><?php echo tgl_indo($plan['due_date']); ?></td>
															<td align="center"><label style="display: block;" class="label <?php echo status_percen(ceil($progress_plan)); ?>"><?php echo ceil($progress_plan); ?>%</label></td>
															<td class="btn-group" width="">
																<a href="detail-planning.php?id=<?php echo $plan['id']; ?>" class="btn btn-primary">View</a>
															</td>
														</tr>
													<?php $no++ ?>
													<?php } ?>
													<?php }else{ ?>
													<tr class="odd gradeX">
														<td colspan="5" align="center">No Data Available</td>
													</tr>
												<?php } ?>	
												</tbody>
											</table>
										</div>
									</div>
								</div>	
							</div>
						</form>
								
					</div>
				</div>
            </div>
        </div>
        <script src="dist/assets/libs/jquery/jquery.min.js"></script>
        <script src="dist/assets/bs3/js/bootstrap.min.js"></script>
        <script src="dist/assets/plugins/jquery-navgoco/jquery.navgoco.js"></script>
        <script src="dist/js/main.js"></script>

        <!--[if lt IE 9]>
        <script src="dist/assets/plugins/flot/excanvas.min.js"></script>
        <![endif]-->
        <script src="dist/assets/plugins/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="includes/js/script.js"></script>

        <script src="dist/assets/plugins/jquery-datatables/js/jquery.dataTables.js"></script>
        <script src="dist/assets/plugins/jquery-datatables/js/dataTables.tableTools.js"></script>
        <script src="dist/assets/plugins/jquery-datatables/js/dataTables.bootstrap.js"></script>
        <script src="dist/assets/plugins/jquery-select2/select2.min.js"></script>
        <script src="includes/js/tables-data-tables.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#status_services").on("click", "#setstatus", function(){
					
					var id 				= $('input[name="idstatus"]').val();
					var status 			= $('select[name="set-status"]').val();
					
					console.log (id);
					console.log (status);
					
					var proceed = true;
					
					if(proceed){
						//get input field values data to be sent to server
						post_data = {
							id,status
						};
						
						//Ajax post data to server
						$.post('action/set_status.php', post_data, function(response){ 
							//console.log('dor');
							if(response.type == 'error'){ 
								output = '<div class="error">'+response.text+'</div>';
								console.log ('error');
							}else{
								output = '<p id="result" class="text-warning">'+response.text+'</p>';
							}
							$("#statushasil").hide().html(output).slideDown();
						}, 'json');
					}
				});
			});
		</script>
    </body>
</html>
<?php } ?>