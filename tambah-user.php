<?php
	session_start();
	include 'core/helper/myHelper.php';
	
	if (!empty($_SESSION['username']) AND !empty($_SESSION['password'])) {
	
		include 'core/init.php';
		
		$body = 'daftar-admin';
?>
<!doctype html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->
	<head>
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<?php include ('_include.php'); ?>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <!--<link rel="shortcut icon" href="/favicon.ico">-->
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="stylesheet" href="dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="dist/css/admin.css">
        <link rel="stylesheet" href="includes/css/style.css">
        <link rel="stylesheet" href="dist/assets/font-awesome/css/font-awesome.css">

        <link rel="stylesheet" href="dist/css/plugins/jquery-select2.min.css">
        <link rel="stylesheet" href="dist/css/plugins/jquery-dataTables.min.css">
        <!--[if lt IE 9]>
        <script src="dist/assets/libs/html5shiv/html5shiv.min.js"></script>
        <script src="dist/assets/libs/respond/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="">
       
	   <?php include ('_header.php'); ?>
	   
        <div class="page-wrapper">
            <aside class="sidebar sidebar-default">
				
				<?php include('nav.php'); ?>
			
			</aside>

            <div class="page-content">
                <div class="page-subheading page-subheading-md">
					<ol class="breadcrumb">
						<li><a href="javascript:;">Dashboard</a></li>
						<li class="active"><a href="javascript:;">Tambah Admin</a></li>
					</ol>
				</div>
				<div class="page-heading page-heading-md">
					<h2 class="pull-left">Admin</h2>
					<div class="col-button-colors pull-right">
						<a href="tambah-pengguna.php" class="btn btn-primary">Kembali</a>
					</div>
					<div class="clearfix"></div>
				</div>

				<form id="userAddForm" action="action/add_admin.php" name="form-tambah-pengguna" method="POST" class="form-horizontal form-bordered" role="form">
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">Tambah Admin</h4>

						</div>
						<div class="panel-body">

							<div class="form-group">
								<label class="control-label col-sm-3">Nama Lengkap</label>

								<div class="controls col-sm-6">
									<input type="text" name="name" class="form-control required" title="Nama Lengkap Harus Di Isi" placeholder="Masukkan Nama">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-3">Username</label>

								<div class="controls col-sm-6">
									<input type="text" name="input_username" class="form-control required"  title="User Lengkap Harus Di Isi" placeholder="Masukkan Username">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-sm-3">Password</label>

								<div class="controls col-sm-6">
									<input type="password" id="input_password" name="input_password"  title="Password Lengkap Harus Di Isi" class="form-control required" placeholder="Masukkan Password">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-sm-3">Ulangi Password</label>

								<div class="controls col-sm-6">
									<input type="password" equalTo="#input_password" name="ulangi-password" class="form-control required"  title="Password Tidak Sama" placeholder="Masukkan Ulang Password">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-sm-3">Email</label>

								<div class="controls col-sm-6">
									<input type="email" name="email" class="form-control" placeholder="Masukkan Email">
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-3">Jenis Kelamin</label>

								<div class="controls col-sm-6">
									<label class="radio-inline">
										<input type="radio" name="jenis-kelamin" value="1">
										Laki-laki
									</label>
									<label class="radio-inline">
										<input type="radio" name="jenis-kelamin" value="0">
										Perempuan
									</label>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-sm-3">Nomer Telepon</label>

								<div class="controls col-sm-6">
									<input type="text" name="no-telepon" class="form-control" placeholder="Masukkan No. Telp">
								</div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-sm-3">Aktif</label>
								
								<div class="controls col-sm-6">
									<input name="user-status" value="1" class="boot-switch" type="checkbox" data-on-color="warning" data-off-color="danger">
								</div>
							</div>
						
							<div class="form-group">
								<div class="controls col-sm-6 col-sm-offset-2">
									<button type="submit" class="btn btn-primary">Submit</button>&nbsp;&nbsp;&nbsp;
									<button type="reset" class="btn btn-primary">Reset</button>
								</div>
							</div>
							
						</div>
					</div>
				</form>

            </div>
        </div>
        <script src="dist/assets/libs/jquery/jquery.min.js"></script>
        <script src="dist/assets/bs3/js/bootstrap.min.js"></script>
        <script src="dist/assets/plugins/jquery-navgoco/jquery.navgoco.js"></script>
        <script src="dist/js/main.js"></script>

        <!--[if lt IE 9]>
        <script src="dist/assets/plugins/flot/excanvas.min.js"></script>
        <![endif]-->
        <script src="dist/assets/plugins/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="includes/js/script.js"></script>

        <script src="dist/assets/plugins/jquery-datatables/js/jquery.dataTables.js"></script>
        <script src="dist/assets/plugins/jquery-datatables/js/dataTables.tableTools.js"></script>
        <script src="dist/assets/plugins/jquery-datatables/js/dataTables.bootstrap.js"></script>
        <script src="dist/assets/plugins/jquery-select2/select2.min.js"></script>
        <script src="includes/js/tables-data-tables.js"></script>
		<script src="dist/assets/plugins/jquery-validation/jquery.validate.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$("#userAddForm").validate();
			})
		</script>


    </body>
</html>
	<?php } ?>