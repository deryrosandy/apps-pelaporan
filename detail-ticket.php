<?php
	session_start();
	include 'core/helper/myHelper.php';
	
	if (!empty($_SESSION['username']) AND !empty($_SESSION['password'])) {
	
		include 'core/init.php';

		$id = $_GET['id'];
		
		$ticket = $db->ticket()->where("id", $id)->fetch();
		//var_dump($ticket['subject']);
		//die();
		$category = $db->project_category();
		
		$body = 'tickets';
	?>

<!doctype html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->
	<head>
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<?php include ('_include.php'); ?>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <!--<link rel="shortcut icon" href="/favicon.ico">-->
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="stylesheet" href="dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="dist/css/admin.css">
        <link rel="stylesheet" href="includes/css/style.css">
        <link rel="stylesheet" href="dist/assets/font-awesome/css/font-awesome.css">
		<link rel="stylesheet" href="dist/css/plugins/jquery-chosen.min.css">
        <link rel="stylesheet" href="dist/css/plugins/jquery-select2.min.css">
        <link rel="stylesheet" href="dist/css/plugins/jquery-dataTables.min.css">
        <!--[if lt IE 9]>
        <script src="dist/assets/libs/html5shiv/html5shiv.min.js"></script>
        <script src="dist/assets/libs/respond/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="">
		
	   <?php include ('_header.php'); ?>
	   
        <div class="page-wrapper">
            <aside class="sidebar sidebar-default">
				
				<?php include('nav.php'); ?>
			
			</aside>

            <div class="page-content">
                <div class="page-subheading page-subheading-md">
					<ol class="breadcrumb">
						<li><a href="javascript:;">Dashboard</a></li>
						<li class="active"><a href="javascript:;">Detail Ticket</a></li>
					</ol>
				</div>
				<div class="page-heading page-heading-md">
					<h2 class="pull-left">Detail Ticket</h2>
					<div class="col-button-colors pull-right">
						<a href="tickets.php" class="btn btn-primary">Back</a>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="col-sm-12">
					<div class="col-lg-12">
						<form action="action/proses_komentar.php" name="submit-komentar" method="POST" class="form-horizontal form-bordered" role="form">
							<div class="panel panel-default">
								<div class="panel-heading">
									<div class="row">
										<div class="col-lg-12">
											<div class="col-button-colors pull-left">
												<h1 style="padding-top:10px;" class="panel-title">Detail Ticket</h1>
											</div>
										</div>
									</div>
								</div>
								<div class="panel-body">
									<div id="detail-request-karyawan" style="padding:20px;" class="form-group">
										<div class="form-group">
											<label class="control-label col-sm-3">Subject :</label>
											
											<div class="controls col-sm-3">
												<label class="control-label"><?php echo $ticket['subject']; ?></label>
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-sm-3">Project Manager :</label>

											<div class="controls col-sm-3">
												<label class="control-label"><?php echo $ticket->user['firstname'] . ' ' . $ticket->user['lastname']; ?></label>
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-sm-3">Priority :</label>
											
											<div class="controls col-sm-3">
												<label class="control-label"><?php echo ucfirst($ticket['priority']); ?></label>
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-sm-3">Project Name :</label>

											<div class="controls col-sm-3">
												<label class="control-label"><?php echo $ticket->project['name']; ?></label>
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-sm-3">Task Name :</label>

											<div class="controls col-sm-3">
												<label class="control-label"><?php echo $ticket->task['name']; ?></label>
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-sm-3">Description :</label>

											<div class="controls col-sm-3">
												<text class="control-label" style="text-align: left;"><?php echo $ticket['description']; ?></text>
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-sm-3">Created :</label>

											<div class="controls col-sm-3">
												<text class="control-label" style="text-align: left;"><?php echo tgl_indo($ticket['created']); ?></text>
											</div>
										</div>
										
										<div class="form-group">
											<label class="control-label col-sm-3">Status :</label>

											<div class="controls col-sm-2">
												<h4><label class="label <?php echo colour_status($ticket['status']); ?>" style="text-align: left;"><?php echo $ticket['status']; ?></label></h4>
											</div>
											<?php if(($_SESSION['user_type']) != 'manajer-proyek'){ ?>
											<div id="update_status_ticket" class="controls col-sm-3 pull-left">
												<div class="input-group">
													<select name="set-status" class="form-control form-chosen required" data-placeholder="" aria-required="true">
														<option value="">- Set Status -</option>
														<option value="on process">On Process</option>
														<option value="resolved">Resolved</option>
														<option value="cancel">Cancel</option>
													</select>
													<span class="input-group-btn">
														<input type="hidden" name="idstatus" value="<?php echo $ticket['id']; ?>" class="btn btn-primary" type="button">Save</button>
														<span id="setstatus" class="btn btn-primary" type="submit">Submit</span>
													</span>
												</div>
												<div class="input-group">
													<span id="statushasil"></span>
												</div>
											</div>
										<?php } ?>
										</div>
										
										<div class="form-group">
											<label class="control-label col-sm-3">Reply Ticket :</label>
											<div class="controls col-sm-9">
												<text class="control-label" style="text-align: left;width:5px;"><?php //echo $keluhan['notes']; ?></text>
												<div class="row">
													<div class="col-lg-12">
														<?php $comments = $db->ticket_comment()
																->where("ticket_id", $ticket["id"])
																->order("created ASC");
														?>
														<?php foreach ($comments as $komen){ ?>
														<h5 class="text-primary"><?php if ($_SESSION['id'] == $komen->user['id']){ echo 'Saya :'; }else{ echo $komen->user["firstname"] . ' Says :'; }?></h5>
														<div class="row">
															<div class="col-lg-7">
																<p><?php echo $komen["description"]; ?></p>
															</div>
															<div class="col-lg-5">
																<p><?php echo $komen['created']; ?></p>
															</div>
														</div>
														<?php } ?>
													</div>
												</div>
												<div class="row">
													<div class="col-lg-12 mail-message">
														<textarea style="width: 80%;" name="komentar" class="form-control" placeholder="Click here to reply..." style="resize:vertical;"></textarea></br/>
														<input type="hidden" name="id" value="<?php echo $id; ?>"/> 
														<input type="hidden" name="id_ticket" value="<?php echo $ticket["id"]; ?>"/> 
														<button type="submit" class="btn btn-primary btn-md">Kirim</button>
													</div>
												</div>
											</div>
										</div>
										
									</div>
								</div>	
							</div>
						</form>	
					</div>
				</div>
            </div>
        </div>
        <script src="dist/assets/libs/jquery/jquery.min.js"></script>
        <script src="dist/assets/bs3/js/bootstrap.min.js"></script>
        <script src="dist/assets/plugins/jquery-navgoco/jquery.navgoco.js"></script>
        <script src="dist/js/main.js"></script>

        <!--[if lt IE 9]>
        <script src="dist/assets/plugins/flot/excanvas.min.js"></script>
        <![endif]-->
        <script src="dist/assets/plugins/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="includes/js/script.js"></script>

        <script src="dist/assets/plugins/jquery-datatables/js/jquery.dataTables.js"></script>
        <script src="dist/assets/plugins/jquery-datatables/js/dataTables.tableTools.js"></script>
        <script src="dist/assets/plugins/jquery-datatables/js/dataTables.bootstrap.js"></script>
        <script src="dist/assets/plugins/jquery-select2/select2.min.js"></script>
		<script src="dist/assets/plugins/jquery-chosen/chosen.jquery.min.js"></script>
		<script src="dist/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="includes/js/tables-data-tables.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
				$("#update_status_ticket").on("click", "#setstatus", function(){
					
					var id 				= $('input[name="idstatus"]').val();
					var status 			= $('select[name="set-status"]').val();
					
					console.log (id);
					console.log (status);
					
					var proceed = true;
					
					if(proceed){
						//get input field values data to be sent to server
						post_data = {
							id,status
						};
						
						//Ajax post data to server
						$.post('action/set_status.php', post_data, function(response){ 
							//console.log('dor');
							if(response.type == 'error'){ 
								output = '<div class="error">'+response.text+'</div>';
								console.log ('error');
							}else{
								output = '<p id="result" class="text-warning">'+response.text+'</p>';
							}
							$("#statushasil").hide().html(output).slideDown();
						}, 'json');
					}
				});
			});
		</script>
    </body>
</html>
<?php } ?>