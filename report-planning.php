<?php
	session_start();
	include 'core/helper/myHelper.php';
	
	if (!empty($_SESSION['username']) AND !empty($_SESSION['password'])) {
	
		include 'core/init.php';
		
		require_once("assets/pdf/fpdf/fpdf.php");

class FPDF_AutoWrapTable extends FPDF {
  	private $data = array();
  	private $options = array(
  		'filename' => '',
  		'destinationfile' => '',
  		'paper_size'=>'A4',
  		'orientation'=>'P'
  	);
  	
  	function __construct($data = array(), $options = array()) {
    	parent::__construct();
    	$this->data = $data;
    	$this->options = $options;
	}
	
	public function rptDetailData () {
		//
		$border = 0;
		$this->AddPage();
		$this->SetAutoPageBreak(true,60);
		$this->AliasNbPages();
		$left = 25;
		
		//header
		$this->SetFont("", "B", 15);
		//$this->MultiCell(0, 12, 'PT. TUNAS DAIHATSU');
		$this->Cell(0, 1, " ", "B");
		$this->Ln(30);
		$this->SetFont("", "B", 12);
		$this->SetX($left); $this->Cell(0, 10, 'PLANNING REPORT', 0, 1,'C');
		$this->Ln(20);
		
		$h = 20;
		$left = 40;
		$top = 80;	
		$this->SetFont('Arial','B',10);
		#tableheader
		$this->SetFillColor(200,200,200);	
		$left = $this->GetX();
		
		mysql_connect("localhost","root","");
		mysql_select_db("service_daihatsu_db");
		
		$data = array();
		
		$query = "SELECT services.user_id, services.*, user.id, user.name FROM services INNER JOIN `user` ON services.user_id=`user`.id";
		$sql = mysql_query($query);
		while($row =mysql_fetch_assoc($sql)){
			
		}
		
		$this->Cell(150,$h,'Pengirim',0,0,'l',false);
		$this->SetX($left += 150); $this->Cell(380,$h,'Subject',1,0,'C',true);
		$this->Ln(50);
		
		//$this->SetX($left += 20); $this->Cell(75, $h, 'NIP', 1, 0, 'C',true);
		$this->Cell(20,$h,'No.',1,0,'C',true);
		$this->SetX($left += 20); $this->Cell(75,$h,'Pengirim',1,0,'C',true);
		$this->SetX($left += 75); $this->Cell(80,$h,'Subject',1,0,'C',true);
		$this->SetX($left += 80); $this->Cell(80,$h,'No. Polisi',1,0,'C',true);
		$this->SetX($left += 80); $this->Cell(70,$h,'Type',1,0,'C',true);
		$this->SetX($left += 70); $this->Cell(80,$h,'Warna',1,0,'C',true);
		$this->SetX($left += 80); $this->Cell(100,$h,'No. Mesin',1,0,'C',true);
		$this->SetX($left += 100); $this->Cell(100,$h,'No. Rangka',1,0,'C',true);
		$this->SetX($left += 100); $this->Cell(70,$h,'Status',1,0,'C',true);
		$this->SetX($left += 70); $this->Cell(100,$h,'Alamat',1,0,'C',true);
		$this->SetX($left += 100); $this->Cell(100,$h,'Keterangan',1,1,'C',true);
		//$this->Ln(20);
			
		$this->SetFont('Arial','',9);
		$this->SetWidths(array(20,75,80,80,70,80,100,100,70,100,100));
		$this->SetAligns(array('C','L','L','L','L','L'));
		
		$no = 1; $this->SetFillColor(255);
		foreach ($this->data as $baris) {
			$this->Row(
				array($no++, 
				$baris['name'], 
				$baris['subject'], 
				$baris['no_pol'], 
				$baris['car_type'], 
				$baris['colour'],
				$baris['machine_number'],
				$baris['case_number'],
				$baris['status'],
				$baris['address'],
				$baris['description']
			));
		}
	}

	public function printPDF () {
				
		if ($this->options['paper_size'] == "A4") {
			$a = 8.3 * 72; //1 inch = 72 pt
			$b = 13.0 * 72;
			$this->FPDF($this->options['orientation'], "pt", array($a,$b));
		} else {
			$this->FPDF($this->options['orientation'], "pt", $this->options['paper_size']);
		}
		
	    $this->SetAutoPageBreak(false);
	    $this->AliasNbPages();
	    $this->SetFont("helvetica", "B", 10);
	    //$this->AddPage();
	
	    $this->rptDetailData();
			    
	    $this->Output($this->options['filename'],$this->options['destinationfile']);
  	}
  	
  	
  	
  	private $widths;
	private $aligns;

	function SetWidths($w)
	{
		//Set the array of column widths
		$this->widths=$w;
	}

	function SetAligns($a)
	{
		//Set the array of column alignments
		$this->aligns=$a;
	}

	function Row($data)
	{
		//Calculate the height of the row
		$nb=0;
		for($i=0;$i<count($data);$i++)
			$nb=max($nb,$this->NbLines($this->widths[$i],$data[$i]));
		$h=14*$nb;
		//Issue a page break first if needed
		$this->CheckPageBreak($h);
		//Draw the cells of the row
		for($i=0;$i<count($data);$i++)
		{
			$w=$this->widths[$i];
			$a=isset($this->aligns[$i]) ? $this->aligns[$i] : 'L';
			//Save the current position
			$x=$this->GetX();
			$y=$this->GetY();
			//Draw the border
			$this->Rect($x,$y,$w,$h);
			//Print the text
			$this->MultiCell($w,14,$data[$i],0,$a);
			//Put the position to the right of the cell
			$this->SetXY($x+$w,$y);
		}
		//Go to the next line
		$this->Ln($h);
	}

	function CheckPageBreak($h)
	{
		//If the height h would cause an overflow, add a new page immediately
		if($this->GetY()+$h>$this->PageBreakTrigger)
			$this->AddPage($this->CurOrientation);
	}

	function NbLines($w,$txt)
	{
		//Computes the number of lines a MultiCell of width w will take
		$cw=&$this->CurrentFont['cw'];
		if($w==0)
			$w=$this->w-$this->rMargin-$this->x;
		$wmax=($w-2*$this->cMargin)*1000/$this->FontSize;
		$s=str_replace("\r",'',$txt);
		$nb=strlen($s);
		if($nb>0 and $s[$nb-1]=="\n")
			$nb--;
		$sep=-1;
		$i=0;
		$j=0;
		$l=0;
		$nl=1;
		while($i<$nb)
		{
			$c=$s[$i];
			if($c=="\n")
			{
				$i++;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
				continue;
			}
			if($c==' ')
				$sep=$i;
			$l+=$cw[$c];
			if($l>$wmax)
			{
				if($sep==-1)
				{
					if($i==$j)
						$i++;
				}
				else
					$i=$sep+1;
				$sep=-1;
				$j=$i;
				$l=0;
				$nl++;
			}
			else
				$i++;
		}
		return $nl;
	}
} //end of class
		
		$services = $db->services();
						//->where("user_login_id", $user_group_id);
		mysql_connect("localhost","root","");
		mysql_select_db("service_daihatsu_db");
		
		$data = array();
		
		$query = "SELECT services.user_id, services.*, user.id, user.name FROM services INNER JOIN `user` ON services.user_id=`user`.id";
		
		$sql = mysql_query($query);
		while($row =mysql_fetch_assoc($sql)){
			array_push($data, $row);
		}
//pilihan
$options = array(
	'filename' => 'report_daftar_services.pdf', //nama file penyimpanan, kosongkan jika output ke browser
	'destinationfile' => 'I', //I=inline browser (default), F=local file, D=download
	'paper_size'=>'A4',	//paper size: F4, A3, A4, A5, Letter, Legal
	'orientation'=>'P' //orientation: P=portrait, L=landscape
);

$tabel = new FPDF_AutoWrapTable($data, $options);
$tabel->printPDF();

}
?>