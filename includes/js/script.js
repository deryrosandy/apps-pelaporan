jQuery(function ($) {
	//Datepicker
    $('[data-rel=datepicker]').datepicker({
        autoclose: true,
        todayHighlight: true
    });
	// Chosen
    $('select.form-chosen').chosen();
});
