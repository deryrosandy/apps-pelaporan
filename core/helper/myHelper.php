<?
function jenis_kel($jenis) {
	
	$jenis_kelamin = array("Perempuan", "Laki-laki");
	
	$result = $jenis_kelamin[$jenis];
	
	return($result);
}

function status($user_type){
	
	$user = array("Tidak Aktif", "Aktif");
	
	$result = $user[$user_type];
	
	return($result);
}

function anti_injection($data){
  $filter = mysqli_real_escape_string(stripslashes(strip_tags(htmlspecialchars($data,ENT_QUOTES))));
  return $filter;
}

function tgl_indo($tgl){
		$tanggal = substr($tgl,8,2);
		$bulan = getBulan(substr($tgl,5,2));
		$tahun = substr($tgl,0,4);
		return $tanggal.' '.$bulan.' '.$tahun;		 
}

function getBulan($bln){
	switch ($bln){
		case 1: 
			return "Januari";
			break;
		case 2:
			return "Februari";
			break;
		case 3:
			return "Maret";
			break;
		case 4:
			return "April";
			break;
		case 5:
			return "Mei";
			break;
		case 6:
			return "Juni";
			break;
		case 7:
			return "Juli";
			break;
		case 8:
			return "Agustus";
			break;
		case 9:
			return "September";
			break;
		case 10:
			return "Oktober";
			break;
		case 11:
			return "November";
			break;
		case 12:
			return "Desember";
			break;
	}
} 

function tgl_indo2($tgl){
		$tanggal = substr($tgl,8,2);
		$bulan = getBulan2(substr($tgl,5,2));
		$tahun = substr($tgl,0,4);
		return $tanggal.' '.$bulan.' '.$tahun;		 
}

function getBulan2($bln){
	switch ($bln){
		case 1: 
			return "Jan";
			break;
		case 2:
			return "Feb";
			break;
		case 3:
			return "Mar";
			break;
		case 4:
			return "Apr";
			break;
		case 5:
			return "Mei";
			break;
		case 6:
			return "Jun";
			break;
		case 7:
			return "Jul";
			break;
		case 8:
			return "Agus";
			break;
		case 9:
			return "Sep";
			break;
		case 10:
			return "Okt";
			break;
		case 11:
			return "Nov";
			break;
		case 12:
			return "Des";
			break;
	}
} 

function colour_status($status){
	switch ($status){
		case "cancel": 
			return "label-danger";
			break;
		case "assigned":
			return "label-primary";
			break;
		case "resolved":
			return "label-success";
			break;
		case "on process":
			return "label-warning";
			break;
	}
}
function colour_status2($status){
	switch ($status){
		case "not yet": 
			return "label-danger";
			break;
		case "on progress":
			return "label-warning";
			break;
		case "completed":
			return "label-success";
			break;
	}
}
function status_percen($persen){
	switch ($persen){
		case 100: 
			return "label-success";
			break;
		case 0: 
			return "label-danger";
			break;
		default:
			return "label-warning";
			break;
	}
}
function login_validate() {
	$timeout = 600; 
	$_SESSION["expires_by"] = time() + $timeout;
}
function login_check() {
	$exp_time = $_SESSION["expires_by"];
	if (time() < $exp_time) {
		login_validate();
		return true; 
	} else {
		unset($_SESSION["expires_by"]);
		return false;
	}
}
?>