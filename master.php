<?php
	session_start();
	
	if (!empty($_SESSION['username']) AND !empty($_SESSION['password'])){
	
		include 'core/init.php';
		include 'core/helper/myHelper.php';
		
		//$module = "";
		if (!empty($_GET['module'] == 'category')){
			$module = 'category';
		}else{
			$module= 'users';
		};
		
		$users = $db->user();
		$project_category = $db->project_category();
		
		$body = 'master';
?>

<!doctype html>
<!--[if IE 8]>         <html class="ie8"> <![endif]-->
<!--[if IE 9]>         <html class="ie9"> <![endif]-->
<!--[if gt IE 9]><!--> <html> <!--<![endif]-->
	<head>
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<?php include ('_include.php'); ?>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">
        <!--<link rel="shortcut icon" href="/favicon.ico">-->
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link rel="stylesheet" href="dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="dist/css/admin.css">
        <link rel="stylesheet" href="includes/css/style.css">
        <link rel="stylesheet" href="dist/assets/font-awesome/css/font-awesome.css">

        <link rel="stylesheet" href="dist/css/plugins/jquery-select2.min.css">
		<link rel="stylesheet" href="dist/css/plugins/jquery-chosen.min.css">
        <link rel="stylesheet" href="dist/css/plugins/jquery-dataTables.min.css">
        <!--[if lt IE 9]>
        <script src="dist/assets/libs/html5shiv/html5shiv.min.js"></script>
        <script src="dist/assets/libs/respond/respond.min.js"></script>
        <![endif]-->

    </head>
    <body class="">
		
		<?php include '_header.php'; ?>
		
        <div class="page-wrapper">
            <aside class="sidebar sidebar-default">
				
				<?php include('nav.php'); ?>
			
			</aside>

            <div class="page-content">
                <div class="page-subheading page-subheading-md">
					<ol class="breadcrumb">
						<li><a href="javascript:;">Dashboard</a></li>
						<li class="active"><a href="javascript:;">Master</a></li>
					</ol>
				</div>
				<div class="page-heading page-heading-md">
					<h3 class="pull-left">Master Data</h3>
					<div class="col-button-colors pull-right">
						<a href="dashboard.php" class="btn btn-primary">Back</a>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="container-fluid-md">
					<div class="row">
						<div class="col-lg-11">
							<ul class="nav nav-pills">
								<li class="<?php echo $module == 'users' ? 'active' : ''; ?>"><a data-toggle="" href="master.php?module=users"><strong>USER MANAGEMENT</a></strong></li>
								<li class="<?php echo $module == 'category' ? 'active' : ''; ?>"><a data-toggle="" class="" href="master.php?module=category"><strong>CATEGORY PROJECT</a></strong></li>
							</ul>
							
							<?php switch($module){
								case "users":
							?>				
							<div class="tab-content panel panel-default">
								<div class="panel-heading">
									<div class="row">
										<div class="col-lg-12">
											<div class="col-button-colors pull-left">
												<h1 style="padding-top:10px;" class="panel-title">List User</h1>
											</div>
											<div class="col-button-colors pull-right">
												<a href="add-user.php" style="margin-bottom: 0px;" class="btn btn-primary">Add New User</a>
											</div>
										</div>
									</div>
								</div>
								<div class="panel-body">
									
									<h6>&nbsp;</h6>
									<table id="table-basic" class="table table-item table-striped">
										<thead>
											<tr>
												<th class="no" style="width:30px">No.</th>
												<th class="name">Username</th>
												<th class="area">Name</th>
												<th class="division">Email</th>
												<th class="branch">Phone</th>
												<th class="branch">Status</th>
												<th class="branch">User Type</th>
												<?php if($user_type == 'administrator'){ ?>
												<th class="action">Action</th>
												<?php } ?>
											</tr>
										</thead>
										<tbody>
										<?php $no = 1; ?>
										
										<?php foreach ($users as $usr){ ?>
											<tr class="odd gradeX">
												<td><?php echo $no; ?></td>
												<td><?php echo ucfirst($usr["username"]); ?></td>
												<td><?php echo ucfirst($usr["firstname"]); ?></td>
												<td><?php echo ucfirst($usr["email"]); ?></td>
												<td><?php echo ucfirst($usr["phone_number"]); ?></td>
												<td><?php echo ucfirst(status($usr["status"])); ?></td>
												<td><?php echo ucfirst($usr['user_type']); ?></td>
												<td class="btn-group">
													<a href="edit-user.php?id=<?php echo $usr['id']; ?>" class="btn btn-warning">Edit</a>
													<a href="action/delete-user.php?id=<?php echo $usr['id']; ?>" class="btn btn-danger" onclick="return confirm('Are you sure you want to Delete ?');">Delete</a>
												</td>
											</tr>
										<?php $no++ ?>
										<?php } ?>
										</tbody>
									</table>
								</div>
							</div>
							
							<?php break; ?>
							
							<?php 	case "category": ?>
							<div class="col-lg-12 col-md-12">
								<div class="tab-content panel panel-default">
									<div class="panel-heading">
										<div class="row">
											<div class="col-lg-12">
												<div class="col-button-colors pull-left">
													<h1 style="padding-top:10px;" class="panel-title">List Category Project</h1>
												</div>
												<div class="col-button-colors pull-right">
													<a href="add-category.php" style="margin-bottom: 0px;" class="btn btn-primary">Add New Category</a>
												</div>
											</div>
										</div>
									</div>
									<div class="panel-body">
										<table id="table-basic" class="table table-item table-striped">
											<thead>
												<tr>
													<th class="no" style="width:30px">No.</th>
													<th style="width:250px;" class="name">Name</th>
													<th class="description">Description</th>
													<th style="width:200px;" class="action">Action</th>
												</tr>
											</thead>
											<tbody>
											<?php $no = 1; ?>
											
											<?php foreach ($project_category as $cat){ ?>
												<tr class="odd gradeX">
													<td><?php echo $no; ?></td>
													<td><?php echo ucfirst($cat["name"]); ?></td>
													<td><?php echo ucfirst($cat['description']); ?></td>
													<td class="btn-group">
														<a href="edit-category.php?id=<?php echo $cat['id']; ?>" class="btn btn-warning">Edit</a>
														<a href="action/delete-category.php?id=<?php echo $cat['id']; ?>" class="btn btn-danger" onclick="return confirm('Are you sure you want to Delete ?');">Delete</a>
													</td>
												</tr>
											<?php $no++ ?>
											<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							</div>
							<?php break; ?>
							
							<?php default: ?>
								<div class="tab-content panel panel-default">
									<div class="panel-body">
										<table id="table-basic" class="table table-item table-striped">
											<thead>
												<tr>
													<th class="no" style="width:30px">No.</th>
													<th class="name">Name</th>
													<th class="area">Area</th>
													<th class="division">Division</th>
													<th class="branch">Branch</th>
													<?php if($user_type == 'administrator'){ ?>
													<th class="action">Action</th>
													<?php } ?>
												</tr>
											</thead>
											<tbody>
											<?php $no = 1; ?>
											
											<?php foreach ($items as $item){ ?>
												<tr class="odd gradeX">
													<td><?php echo $no; ?></td>
													<td><?php echo ucfirst($item["item_name"]); ?></td>
													<td><?php echo ucfirst($item->item_area["name"]); ?></td>
													<td><?php echo strtoupper($item->divisi['name']); ?></td>
													<td><?php echo ucfirst($item->branch['name']); ?></td>
													<?php if($user_type == 'administrator'){ ?>
													<td class="btn-group">
														<a href="edit-item.php?id=<?php echo $item['id']; ?>" class="btn btn-warning">Edit</a>
														<a href="delete-item.php?id=<?php echo $item['id']; ?>" class="btn btn-danger" onclick="return confirm('Are you sure you want to Delete ?');">Delete</a>
													</td>
													<?php } ?>
												</tr>
											<?php $no++ ?>
											<?php } ?>
											</tbody>
										</table>
									</div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
            </div>
        </div>
		
        <script src="dist/assets/libs/jquery/jquery.min.js"></script>
        <script src="dist/assets/bs3/js/bootstrap.min.js"></script>
        <script src="dist/assets/plugins/jquery-navgoco/jquery.navgoco.js"></script>
        <script src="dist/js/main.js"></script>

        <!--[if lt IE 9]>
        <script src="dist/assets/plugins/flot/excanvas.min.js"></script>
        <![endif]-->
        <script src="dist/assets/plugins/jquery-sparkline/jquery.sparkline.js"></script>
        <script src="dist/assets/plugins/jquery-datatables/js/jquery.dataTables.js"></script>
        <script src="dist/assets/plugins/jquery-datatables/js/dataTables.tableTools.js"></script>
        <script src="dist/assets/plugins/jquery-datatables/js/dataTables.bootstrap.js"></script>
        <script src="dist/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
        <script src="dist/assets/plugins/jquery-select2/select2.min.js"></script>
		<script src="dist/assets/plugins/jquery-chosen/chosen.jquery.min.js"></script>
        <script src="includes/js/tables-data-tables.js"></script>
		<script src="dist/assets/plugins/jquery-validation/jquery.validate.min.js"></script>
		<script src="includes/js/script.js"></script>
    </body>
</html>

<?php } ?>